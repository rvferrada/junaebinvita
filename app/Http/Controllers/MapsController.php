<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MapsController extends Controller
{

    public function index(Request $request) {

        $this->validate($request, [
            'lat' => 'required',
            'lon' => 'required',
            'pag' => 'required'
        ]);

        $skip = 0;

        if ($request->pag != 1) {
            $skip = $request->pag * 30;
        }

        $distance_sql = '6371000 * ACOS( 
                                SIN(RADIANS(latitude)) * SIN(RADIANS(' . $request->lat . ')) 
                                + COS(RADIANS(longitude - ' . $request->lon . ')) * COS(RADIANS(latitude)) 
                                * COS(RADIANS(' . $request->lat . '))
                                )
                    as distance';

        $places = \DB::table('places')
            ->select(
                \DB::raw($distance_sql . ', id, name, address, commune, region')
            )
            /*->having('distance', '<', 1)*/
            ->skip($skip)
            ->take(50)
            ->orderBy('distance', 'ASC')
            ->get();

        return response()->json($places);
    }
}
