<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestController extends Controller
{
    public function index($lat, $lon, $pag) {

        if ($pag != 1) {
            $skip = $pag * 30;
        } else {
            $skip = 0;
        }

        $distance_sql = '6371000 * ACOS( 
                                SIN(RADIANS(latitude)) * SIN(RADIANS(' . $lat . ')) 
                                + COS(RADIANS(longitude - ' . $lon . ')) * COS(RADIANS(latitude)) 
                                * COS(RADIANS(' . $lat . '))
                                )
                    as distance';

        $places = \DB::table('places')
            ->select(
                \DB::raw($distance_sql . ', id, name, address, region')
            )
            /*->having('distance', '<', 1)*/
            ->orderBy('distance', 'ASC')
            ->skip($skip)
            ->take(30)
            ->get();

        return view('welcome2', ["places" => $places, "lat" => $lat, "lon" => $lon, "pag" => $pag]);
    }

    public function postIndex($lat, $lon, $pag) {

        if ($pag != 1) {
            $skip = $pag * 30;
        } else {
            $skip = 0;
        }

        $distance_sql = '6371000 * ACOS( 
                                SIN(RADIANS(latitude)) * SIN(RADIANS(' . $lat . ')) 
                                + COS(RADIANS(longitude - ' . $lon . ')) * COS(RADIANS(latitude)) 
                                * COS(RADIANS(' . $lat . '))
                                )
                    as distance';

        $places = \DB::table('places')
            ->select(
                \DB::raw($distance_sql . ', id, name, address, region')
            )
            /*->having('distance', '<', 1)*/
            ->skip($skip)
            ->take(30)
            ->orderBy('distance', 'ASC')
            ->get();

        return response()->json($places);
    }
}
