<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>
        <script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
        <script type="text/javascript">
            (function(){
                var content = document.getElementById("geolocation-test");

                if (navigator.geolocation)
                {
                    navigator.geolocation.getCurrentPosition(function(objPosition)
                    {
                        var lon = objPosition.coords.longitude;
                        var lat = objPosition.coords.latitude;

                        window.location.replace("https://www.junaebinvita.cl/maps/" + lat + "/" + lon + "/1");

                    }, function(objPositionError)
                    {
                        switch (objPositionError.code)
                        {
                            case objPositionError.PERMISSION_DENIED:
                                content.innerHTML = "No se ha permitido el acceso a la posición del usuario.";
                                break;
                            case objPositionError.POSITION_UNAVAILABLE:
                                content.innerHTML = "No se ha podido acceder a la información de su posición.";
                                break;
                            case objPositionError.TIMEOUT:
                                content.innerHTML = "El servicio ha tardado demasiado tiempo en responder.";
                                break;
                            default:
                                content.innerHTML = "Error desconocido.";
                        }
                    }, {
                        maximumAge: 75000,
                        timeout: 15000
                    });
                }
                else
                {
                    content.innerHTML = "Su navegador no soporta la API de geolocalización.";
                }
            })();
        </script>
    </head>
    <body>
        <div class="blog-test" id="geolocation-test">No se ha permitido el acceso a la posición del usuario.</div>
    </body>
</html>
