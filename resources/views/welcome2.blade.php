<html>
<head>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.1.0.min.js" integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=" crossorigin="anonymous"></script>
<script type="text/javascript">
    window.onscroll = function(ev) {
        if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
            var separador = "/";
            var arregloDeCadenas = window.location.pathname.split(separador);
            var pag = parseInt(arregloDeCadenas[4]);
            var pag = pag + 1;
                $.ajax({
                        method: "GET",
                        url: "https://www.junaebinvita.cl/ajax/" + <?php echo $lat; ?> + "/" + <?php echo $lon; ?> + "/" + pag,
                })
                        .done(function( obj ) {
                            window.history.pushState("", "", String(pag));
                            $.each(obj, function(index, value) {

                                var disName = "Metros.";
                                
                                if (value.distance >= 1000) {
                                    value.distance = value.distance / 1000;
                                    var disName = "Kilómetros.";
                                }

                                $("#content").append("<h3>[" + value.id + "] " +  value.name + "</h3>" +
                                        value.address + "<br>" +
                                        value.region + "<br>" +
                                        parseFloat(value.distance).toFixed(2) + " " + disName);
                            });
                        })
        }
    };
</script>
<div id="content">
    @foreach ($places as $place)
        <h3>[{{ $place->id }}] {{ $place->name }}</h3>
        {{ $place->address }} <br>
        {{ $place->region }} <br>
        {{ (int) ($place->distance >= 1000 ? $place->distance / 1000 : $place->distance) }}
    @endforeach
</div>
</body>
</html>
