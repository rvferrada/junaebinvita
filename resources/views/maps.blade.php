<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>..:: Junaebinvita ::. Lista de locales que aceptan junaeb.</title>

    <!-- Title fonts -->
    <link href="https://fonts.googleapis.com/css?family=Courgette" rel="stylesheet">

    <!-- Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav>
    <div class="nav-wrapper">
        <a href="{{ config('app.url') }}" class="brand-logo center" style="font-family: 'Courgette', cursive;">Junaebinvita</a>
    </div>
</nav>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>

<!-- maps script -->
<script type="text/javascript">
    function generateContainer(showUrl, lat, lon, pag) {
        $.ajax({
            method: "POST",
            url: showUrl,
            data: { _token: "<?php echo csrf_token(); ?>", lat: lat, lon: lon, pag: pag }
        })
                .done(function( obj ) {
                    window.history.pushState("", "", String(showUrl));

                    $.each(obj, function(index, value) {
                        var time = Math.round(value.distance / 85);
                        var disName = "m";

                        if (value.distance >= 1000) {
                            value.distance = value.distance / 1000;
                            var disName = "km";
                        }

                        $(".container").append(
                                "<div class=\"divider\"></div>" +
                                "<div class=\"section\">" +
                                "<div class=\"row\">" +
                                "<div class=\"col s12 m9 l9\">" +
                                "<h5>" + value.name + "</h5>" +
                                "<p>" + value.address + "</p>" +
                                "<p>" + value.commune + " / " + value.region + "</p>" +
                                "<a class=\"waves-effect waves-light btn\"><i class=\"material-icons left\">directions_walk</i>Indicaciones</a>" +
                                "<input type=\"hidden\" value=\"" + value.id + "\">" +
                                "</div>" +
                                "<div class=\"col s12 m3 l3\" style=\"margin-top:20px\">" +
                                "<h5 style=\"text-align: left; color:#43a047\">" + time + " min</h5>" +
                                "<h6 style=\"text-align: left;font-size:20px\">" + Math.round(value.distance) + " " + disName + "</h6>" +
                                "</div>" +
                                "</div>" +
                                "</div>"
                        );
                    });

                })
    }

    $( document ).ready(function() {

        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(function(objPosition)
            {
                var lon = objPosition.coords.longitude;
                var lat = objPosition.coords.latitude;

                var split = "/";
                var url = window.location.pathname.split(split);
                var pag = parseInt(url[4]);

                var showUrl = "https://www.junaebinvita.cl/maps/" + lat + "/" + lon + "/1";

                if (!isNaN(pag)) {
                    var showUrl = "https://www.junaebinvita.cl/maps/" + lat + "/" + lon + "/" + pag;
                }

                generateContainer(showUrl, lat, lon, pag);

                window.onscroll = function(ev) {
                    if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
                        var split = "/";
                        var url = window.location.pathname.split(split);
                        var pag = parseInt(url[4]) + 1;

                        var showUrl = "https://www.junaebinvita.cl/maps/" + lat + "/" + lon + "/" + pag;

                        generateContainer(showUrl, lat, lon, pag);
                    }
                }

            }, function(objPositionError)
            {
                switch (objPositionError.code)
                {
                    case objPositionError.PERMISSION_DENIED:
                        alert("Haz denegado el acceso a tu ubicación, revisa la configuración de tu navegador e intenta nuevamente.\n" +
                                "\nMás información en http://goo.gl/o1k0Ol");
                        break;
                    case objPositionError.POSITION_UNAVAILABLE:
                        alert("No se ha podido acceder a la información de tu posición.");
                        break;
                    case objPositionError.TIMEOUT:
                        alert("El servicio ha tardado demasiado tiempo en responder.");
                        break;
                    default:
                        alert("Error desconocido.");
                }
            }, {
                maximumAge: 75000,
                timeout: 15000
            });
        }
        else
        {
            alert("No podemos acceder a tu ubicación, actualiza el navegador web.");
        }
    });
</script>
<div class="container">
</div>
</body>
</html>
